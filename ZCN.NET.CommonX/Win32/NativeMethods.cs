using System;
using System.Runtime.InteropServices;
using System.Text;

namespace ZCN.NET.CommonX.Win32
{
    /// <summary>
    ///   操作系统中原生的方法汇总集合类
    /// </summary>
	public static class NativeMethods
	{
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;
		}

		public const int SRCCOPY = 13369376;

		[DllImport("gdi32.dll")]
		public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll")]
		public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);

        [DllImport("gdi32.dll")]
		public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

        [DllImport("gdi32.dll")]
		public static extern bool DeleteDC(IntPtr hDC);

        /// <summary>
        ///   删除对象
        /// </summary>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

        /// <summary>
        ///   选中对象
        /// </summary>
        /// <param name="hDC"></param>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
		public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);


        /// <summary>
        ///   获取当前前台窗体句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll")]
		public static extern IntPtr GetForegroundWindow();

        /// <summary>
        ///   获取当前桌面句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll")]
		public static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll")]
		public static extern IntPtr GetWindowDC(IntPtr hWnd);

        [DllImport("user32.dll")]
		public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("user32.dll")]
		public static extern IntPtr GetWindowRect(IntPtr hWnd, ref NativeMethods.RECT rect);

        /// <summary>
        ///   获取当前激活窗口句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll")]
		public static extern IntPtr GetActiveWindow();

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr GetProcAddress(IntPtr hModule, [MarshalAs(UnmanagedType.LPStr)] string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

        [DllImport("kernel32.dll")]
		public static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll")]
		public static extern int GetCurrentProcessId();

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int GetModuleFileName([In] IntPtr hModule, [Out] StringBuilder lpFilename, [MarshalAs(UnmanagedType.U4)] [In] int nSize);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32.dll")]
		public static extern int GetCurrentThreadId();

		public static bool MethodExists(string moduleName, string methodName)
		{
			IntPtr moduleHandle = NativeMethods.GetModuleHandle(moduleName);
			return !(moduleHandle == IntPtr.Zero) && NativeMethods.GetProcAddress(moduleHandle, methodName) != IntPtr.Zero;
		}
	}
}
